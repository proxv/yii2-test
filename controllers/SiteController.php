<?php

namespace app\controllers;

use app\models\Category;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Url;
use yii\helpers\Html;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /*
     * Remove All zeros after float dot
     * @return array
     */
    public function deleteAllZeros(array $arr) {
        $regex = '/[^1-9.]0/';
        $withoutZeros = preg_replace($regex, '', $arr);
        return $withoutZeros;
    }
    /*
     * Remove All useless zeros after dot
     * @return array
     */
    public function deleteExtraZeros(array $arr) {
        $regex = '/\.?0*$/';
        $withoutExtraZeros = preg_replace($regex, '', $arr );
        return $withoutExtraZeros;
    }
    public function getArray() {
        $model = new Category();
        $category = $model->getCategory();
        $array = [];
        foreach ($category as $cat) {
            $array[$cat->parent_id][] = $cat;
        }
        return $array;
    }
    public static function outTree($parent_id)
    {
        $array = self::getArray();
            if (isset($array[$parent_id])) {
                echo '<ul>';
                foreach ($array[$parent_id] as $cat) {
                    echo '<li>'.Html::a($cat->title, Url::to(['category/view', 'id' => $cat->id])).'</li>';
                    self::outTree($cat->id);
                }
                echo '</ul>';
        }
    }




    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       // echo Url::to(['category/index']);
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
