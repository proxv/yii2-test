<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $title
 * @property int $parent_id
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }
    public function getCategory() {
        return $this->find()->all();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'parent_id' => 'Parent category',
        ];
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $deleteItems = $this->find()->where(['parent_id' => $this->id])->all();
        foreach($deleteItems as $del )
        {
            $del->delete();
        }
        return parent::beforeDelete();
    }
}
