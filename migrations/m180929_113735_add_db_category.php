<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 *jClass m180929_113735_add_db_category
 */
class m180929_113735_add_db_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING,
            'parent_id' => Schema::TYPE_INTEGER


        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180929_113735_add_db_category cannot be reverted.\n";

        return false;
    }
    */
}
